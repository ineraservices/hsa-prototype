#!/usr/bin/env bash

#echo "# path to me --->  ${0}     "
#echo "# parent path -->  ${0%/*}  "
#echo "# my name ------>  ${0##*/} "

cd "${0%/*}" # fallback
# cd "${1}" # fallback
# cd "${2}"

# if [[ ! -d $1 ]]; then
# 	echo Pass the full path of the folder you want to make your SOURCE.
# 	exit 1
# fi
#
# if [[ ! -d $2 ]]; then
# 	echo Pass the full path of the folder you want to make your TARGET.
# 	exit 1
# fi
#
# SOURCE=${1}
# TARGET=${2}

SOURCE="${0%/*}/../../hsa/ui-search/dist"
# SOURCE=/some/path
# TARGET=$HOME/workdir
TARGET="${0%/*}"

if [[ ! -d $SOURCE ]] || [[ ! -d $TARGET ]]; then
	echo Either "$SOURCE" or "$TARGET" is not a valid folder.
	exit 1
fi

length=${#SOURCE}
last_char=${SOURCE:length-1:1}
[[ $last_char != "/" ]] && SOURCE="$SOURCE/"; :

length=${#TARGET}
last_char=${TARGET:length-1:1}
[[ $last_char != "/" ]] && TARGET="$TARGET/"; :

cd "$TARGET"

echo "$SOURCE"
echo "$TARGET"

hash rsync 2>/dev/null || { echo >&2 "You need rsync, install with 'brew install rsync'.  Aborting."; exit 1; }

RSYNC_PARAMS="-vaE --progress --exclude=\".DS_Store\" --exclude=\"._*\" --exclude=\"*.tmp\" --exclude=\"/_*\" --exclude=\"*.sh\" --exclude=\"junk/\" --exclude=\"JUNK/\""

function pause(){
	read -p "$*"
}

echo -e 'You will copy everything from\n'$SOURCE'\nto\n'$TARGET'\nCommand: rsync '$RSYNC_PARAMS' '"$SOURCE" "$TARGET"'\nPress any key to test a dry-run first...'
rsync --dry-run $RSYNC_PARAMS "$SOURCE" "$TARGET"

echo "You will now perform a real copy..."

# Tip - this script can be run unattended. Just do `yes | script.sh`.  Add `/dev/null 2>&1` for no output (to mail if using cron).

read -r -p "Are you sure? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
	# echo YES - you answered $response
	# echo "Hi $(whoami)!"

	# Erase every file in target folder except this script file
	find . -type f ! -iname "${0##*/}" -delete

	rsync $RSYNC_PARAMS "$SOURCE" "$TARGET"

	# Replace physical path with relative path since we are no longer in web root
	find . \( -name '*.js' -o -name '*.css' -o -name '*.html' \) -exec sed -i '' 's#=\"\/#="./#g' {} +
	find . -name '*.html' -exec sed -i '' 's#=\/#=./#g' {} +

	find $SOURCE -exec echo Processing \"{}\" \;
else
	# echo NO - you answered $response
	echo Canceled by user.
	exit
fi

# # rsync command parameters
#
# -v, --verbose
# This option increases the amount of information you are given during the transfer. By default, rsync works silently.
#
# -a, --archive
# This is equivalent to -rlptgoD. It is a quick way of saying you want recursion and want to preserve almost everything (with -H being a notable omission).
#
# -E, --executability
# This option causes rsync to preserve the executability (or non-executability) of regular files when --perms is not enabled.
#
# --delete
# This tells rsync to delete extraneous files from the receiving side (ones that aren’t on the sending side), but only for the directories that are being synchronized.
