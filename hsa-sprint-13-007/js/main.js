// Prototyping START

// Stop default link click behavior for nav-links
$('.mock-link, .pointer-link>.toggle-sidebar').click(function(e) {
	e.stopPropagation();
	e.preventDefault();
});

/* Create a popover for mock-links */

var popover_options = {
	content: '<h4>Fungerar inte i prototyp</h4><p>Kommer att fungera i skarp version!</p>',
	html: true,
	placement: 'auto'
};

$('.mock-message').popover(popover_options);

/* Auto hide timer for popover */

$('.mock-message').click(function(e) {
	setTimeout(function() {
		$('[data-original-title]').popover('hide');
	}, 6000);
});

/* Dismiss all popovers by clicking outside */

$('html').on('click', function(e) {
	if (typeof $(e.target).data('original-title') == 'undefined') {
		$('[data-original-title]').popover('hide');
	}
});

$("a.mock-link").click(function() {
	// remove classes from all
	$("a.mock-link").removeClass("active");
	// add class to the one we clicked
	$(this).addClass("active");
});

// Prototyping STOP

function onBlur(el) {
	if (el.value == '') {
		el.value = el.defaultValue;
	}
}

function onFocus(el) {
	if (el.value == el.defaultValue) {
		el.value = '';
	}
}

// Add onblur & onfocus to all form fields...
$(function() {
	$('input').attr('onblur', 'onBlur(this)').attr('onfocus', 'onFocus(this)');
});

// Detailed search - toggle disabled fields START

// Start simple, enable all fields

// $('input[id^=searchCategory]').click(function () {
//   $('input[id^=searchField]').prop('disabled', false);
// });

// Person
// Toggle disable
$('#searchCategory1').click(function() {
	$('#searchField02,#searchField10').prop('disabled', function(i, v) {
		return !v;
	});
});

// Unit
$('#searchCategory2').click(function() {
	$('#searchField01,#searchField02,#searchField03').prop('disabled', function(i, v) {
		return !v;
	});
});

// Locality
$('#searchCategory3').click(function() {
	$('#searchField02,#searchField08,#searchField10').prop('disabled', function(i, v) {
		return !v;
	});
});

// Detailed search - toggle disabled fields STOP

$(function() {
	if ($(".selectpicker").length) {
		$(".selectpicker").selectpicker({
			noneSelectedText: 'Välj något' // by this default 'Nothing selected' --> will change to whatever you enter here
		});
	}
});

$('.toggle-sidebar').on('click', function() {
	$('#sidebar-left').toggleClass('active');
	$('.pointer-link>.toggle-sidebar').toggleClass('opened closed');
});
$('.toggle-filters').on('click', function() {
	$('#filter-submenu').toggleClass('show');
	$('#left-nav-item-01>.dropdown-toggle').attr('aria-expanded', 'true');
});

// Treeview - START

$('#expand-all-branches').on('click', function (e) {
  $('ul.tree-view i.fa-caret-right').removeClass('fa fa-caret-right').addClass('fa fa-chevron-down');
  $('ul.tree-view ul').show();
  $('#collapse-all-branches').removeClass('d-none').addClass('d-block');
  $(this).removeClass('d-block').addClass('d-none');
  return false;
});

$('#collapse-all-branches').on('click', function (e) {
  $('ul.tree-view i.fa-chevron-down').removeClass('fa fa-chevron-down').addClass('fa fa-caret-right');
  $('ul.tree-view ul').hide();
  $('#expand-all-branches').removeClass('d-none').addClass('d-block');
  $(this).removeClass('d-block').addClass('d-none');
  return false;
});

$('ul.tree-view li').on('click', function (e) {
  if ($(this).has("ul").length) {
    if ($(this).find('ul:first').is(":visible")) {
      $(this).find('i:first').removeClass('fa fa-chevron-down').addClass('fa fa-caret-right');
      $(this).find('ul:first').hide();
      e.stopPropagation();
    } else {
      $(this).find('ul:first').show();
      $(this).find('i:first').removeClass('fa fa-caret-right').addClass('fa fa-chevron-down');
      e.stopPropagation();
    }
  } else {
    e.stopPropagation();
  }
});

$('ul.tree-view li a').on('click', function (e) {
  $('ul.tree-view li a.selectedDoc').removeClass('selectedDoc');
  $(this).addClass('selectedDoc');
  //get Doc ID
  var thisDocID = $(this).attr("data-docid");
  alert("clicked Document " + thisDocID);
  return false;
});

// Treeview - STOP
