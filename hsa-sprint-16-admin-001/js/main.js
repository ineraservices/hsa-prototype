// Prototyping START

var prototype_message =
	'<div class="row no-gutters justify-content-center" id="prototype-message-placeholder"><div class="d-flex align-items-center justify-content-center"><div class="post-it"><div class="inner" data-toggle="modal" data-target=".modal"><a href="#" data-toggle="modal" data-target=".modal">Om den här prototypen...</a></div></div></div></div>' +
	'<div class="modal fade">' +
	'	<div class="modal-dialog modal-dialog-centered prototype-message">' +
	'		<div class="modal-content">' +
	'			<div class="modal-header">' +
	'				<h4 class="modal-title">Prototyp 2019-02-27 19:55:27</h4>' +
	'				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
	'			</div>' +
	'			<div class="modal-body">' +
	// '				<p>One fine body&hellip;</p>' +
	'				<h3>Vidareutveckla "bläddra"</h3>' +
	'				<p><em>Sprint 16 &raquo; HSA-362: <a href="https://riv-ta.atlassian.net/browse/HSA-362">https://riv-ta.atlassian.net/browse/HSA-362</a></em></p>' +
	'				<h3>Beskrivning</h3>' +
	'				<p>Tv&aring; referensgrupper &ndash; en med administrat&ouml;rsbeh&ouml;righeter och en grupp med anv&auml;ndare utan s&auml;rskilda beh&ouml;righeter &ndash; har f&aring;tt l&auml;mna synpunkter p&aring; gr&auml;nssnittets vyer <em>enkel s&ouml;k</em>, <em>detaljerad s&ouml;k</em>, <em>resultatlista</em> och <em>bl&auml;ddra</em> vid tv&aring; referensgruppsdemos under februari. Vi genomf&ouml;r &auml;ndringar enligt &ouml;nskem&aring;l och visar nya versioner av dessa vyer vid kommande m&ouml;ten med referensgrupperna.</p>' +
	'				<h4>H&auml;r vill vill de tv&aring; grupperna se nya f&ouml;rslag</h4>' +
	'				<ol>' +
	'				<li>F&ouml;rtydliga vad siffran st&aring;r f&ouml;r (hitta l&auml;mpligt ordval d&aring; "objekt" inte &auml;r en del av generellt ordf&ouml;rr&aring;d --&gt; terminolog)</li>' +
	'				<li>Ta bort knapparna "f&ouml;rhandsgranska" och "detaljvy" (visa fler varianter)</li>' +
	'				<li>G&ouml;r objektnamnet klickbart och l&aring;t detta visa den korta detaljvyn (enbart pilarna anv&auml;nds d&aring; f&ouml;r att veckla ut strukturen)</li>' +
	'				<li>Tydligg&ouml;r att objektnamnen &auml;r klickbara</li>' +
	'				<li>Minska storleken (grad p&aring; text) p&aring; &ouml;versta niv&aring;n</li>' +
	'				<li>Ta bort &rdquo;F&auml;ll ut alla grenar i tr&auml;det&rdquo;. Det skulle i s&aring; fall f&auml;lla ut c:a 750 000 objekt&hellip;</li>' +
	'				</ol>' +
	'				<h4>Testa olika alternativ</h4>' +
	'				<ol>' +
	'				<li>Ta bort siffror f&ouml;r antalet underliggande objekt</li>' +
	'				<li>Veckla ut strukturen n&auml;r objektnamnet klickas, L&auml;gg till en knapp "visa" som visar den korta detaljvyn</li>' +
	'				</ol>' +
	'				</div>' +
	'			</div>' +
	'	</div>' +
	'</div>';

$("body").prepend(prototype_message);
$("#prototype-message-placeholder").click(function () {
  $(this).remove();
});

$(function () {
  setTimeout(function () {
    $('#prototype-message-placeholder').toggleClass('fade-2-visible fade-2-hidden');
  }, 5000);
});

/* Create a popover for mock-links */

var mock_message_popover_options = {
	content: '<h4>Fungerar inte i prototyp</h4><p>Kommer att fungera i skarp version!</p>',
	html: true,
	placement: 'auto'
};

$('.mock-message').popover(mock_message_popover_options);

/* Auto hide timer for prototype popover */

$('.mock-message').click(function(e) {
	setTimeout(function() {
		$('[data-original-title]').popover('hide');
	}, 5000);
});

$(".mock-link").click(function() {
	// remove classes from all
	$(".mock-link").removeClass("active");
	// add class to the one we clicked
	$(this).addClass("active");
});

// Prototyping STOP

// popover setting START

/* Where to show the message? */

$(function() {
	$('.general-message').popover({
		container: 'body'
	})
})

/* Dismiss all popovers by clicking outside */

$('html').on('click', function(e) {
	if (typeof $(e.target).data('original-title') == 'undefined') {
		$('[data-original-title]').popover('hide');
	}
});

/* Auto hide timer for general-message popover */

$('.general-message').click(function(e) {
	setTimeout(function() {
		$('[data-original-title]').popover('hide');
	}, 5000);
});

// popover setting STOP

function onBlur(el) {
	if (el.value == '') {
		el.value = el.defaultValue;
	}
}

function onFocus(el) {
	if (el.value == el.defaultValue) {
		el.value = '';
	}
}

// Add onblur & onfocus to all form fields...
$(function() {
	$('input').attr('onblur', 'onBlur(this)').attr('onfocus', 'onFocus(this)');
});

// Detailed search - toggle disabled fields START

// Start simple, enable all fields

// $('input[id^=searchCategory]').click(function () {
//   $('input[id^=searchField]').prop('disabled', false);
// });

// Toggle disable
// Person
$('#searchCategory1').click(function() {
	$('#searchField01,#searchField03,#searchField05').prop('disabled', function(i, v) {
		return !v;
	});
	// $('#searchField08').next().toggleClass('disabled');
});

// Unit
$('#searchCategory2').click(function() {
	$('#searchField08').prop('disabled', function(i, v) {
		return !v;
	});
	$('#searchField08').next().toggleClass('disabled');
});

// Commission
$('#searchCategory3').click(function() {
	$('#searchField02,#searchField04,#searchField10').prop('disabled', function(i, v) {
		return !v;
	});
	$('#searchField10').next().toggleClass('disabled');
});

// Detailed search - toggle disabled fields STOP

$(function() {
	if ($(".selectpicker").length) {
		$(".selectpicker").selectpicker({
			noneSelectedText: 'Välj något' // by this default 'Nothing selected' --> will change to whatever you enter here
		});
	}
});

$('.toggle-sidebar').on('click', function() {
	$('#sidebar-left').toggleClass('active');
	$('.pointer-link>.toggle-sidebar').toggleClass('opened closed');
});
$('.toggle-filters').on('click', function() {
	$('#filter-submenu').toggleClass('show');
	$('#left-nav-item-01>.dropdown-toggle').attr('aria-expanded', 'true');
});

// Treeview - START

$('#expand-all-branches').on('click', function(e) {
	$('ul.tree-view i.fa-caret-right').removeClass('fa fa-caret-right').addClass('fa fa-chevron-down');
	$('ul.tree-view ul').show();
	$('#collapse-all-branches').removeClass('d-none').addClass('d-block');
	$(this).removeClass('d-block').addClass('d-none');
	return false;
});

$('#collapse-all-branches').on('click', function(e) {
	$('ul.tree-view i.fa-chevron-down').removeClass('fa fa-chevron-down').addClass('fa fa-caret-right');
	$('ul.tree-view ul').hide();
	$('#expand-all-branches').removeClass('d-none').addClass('d-block');
	$(this).removeClass('d-block').addClass('d-none');
	return false;
});

// $(document).on('show.bs.modal', '#exampleModalCenter', function () {
//   alert('#exampleModalCenter');
//   //Do stuff here
// });

$('ul.tree-view li').on('click', function(e) {
	if ($(this).has("ul").length) {
		if ($(this).find('ul:first').is(":visible")) {
			$(this).find('i:first').removeClass('fa fa-chevron-down').addClass('fa fa-caret-right');
			$(this).find('ul:first').hide();
			e.stopPropagation();
		} else {
			$(this).find('ul:first').show();
			$(this).find('i:first').removeClass('fa fa-caret-right').addClass('fa fa-chevron-down');
			e.stopPropagation();
		}
	} else {
		e.stopPropagation();
	}
});

$('ul.tree-view li a').on('click', function(e) {
	$('ul.tree-view li a.selectedDoc').removeClass('selectedDoc');
	$(this).addClass('selectedDoc');
	// get Doc ID
	var thisDocID = $(this).attr("data-docid");
	alert("clicked Document " + thisDocID);
	return false;
});

// Treeview - STOP

// extend browse view - start

this.addClasses = function(parent, level) {
	// add a class to all <li> elements on this level:
	parent.children("li").addClass("cat-level-" + level);
	// find any child <ul> elements and run this function on them,
	// making sure to increment the level counter:
	if (parent.children("li").children("ul").length) {
		this.addClasses(parent.children("li").children("ul"), level + 1);
	}
};
// start off by selecting only the top-level <ul> elements:
this.addClasses($('ul.dynamic-headings'), 1);

$('ul.tree-view li').filter(function() {
	if ($(this).has("ul").length) {
		$(this).addClass('has-children');
	} else {
		$(this).addClass('has-no-child');
	}
});

// change top-level <li> into heading:
$('li.cat-level-1').contents().filter(function() {
	return this.nodeType === 3 && $.trim(this.nodeValue).length;
}).wrap('<h5 class="obj_name" />');

// hide detail-view for now:
$('.detail-view').children().addClass('d-none');

$('li.obj_o').contents().filter(function() {
	return this.nodeType === 3 && $.trim(this.nodeValue).length;
}).wrap('<span class="obj_name" />');

$('li.has-no-child>.obj_name').wrap('<a href="#" />');

/* $(".obj_name").after('<a href="../path/to/unit/or/employee/" class="attributes-detail-view">visa</a><span class="number-of-objects num-gen"></span><span class="attributes-quick-view text-nowrap"><i class="fa fa-file-alt"></i>kortversion</span>') */

$(".obj_name").wrap('<a href="../path/to/unit/or/employee/" class="attributes-detail-view" />').after('<span class="number-of-objects num-gen"></span>')

$(".obj_name").click(function() {
	window.location.href = "../path/to/unit/or/employee/";
});

$("ul.tree-view .attributes-quick-view").click(function(e) {
	$('#short-view-modal').modal('show')
	e.stopPropagation(); // This prevents branch to expand
});

function getNumber() {
	$('.num-gen').each(function() {
		var minNumber = 5;
		var maxNumber = 200;
		var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
		//$(this).html("<b>" + randomnumber + "</b> objekt");
		$(this).html(randomnumber);
	});
}

getNumber();

$("ul.tree-view li:has(> ul)").prepend('<i class="fa fa-caret-right"></i>');

// extend browse view - stop

// select2.org - start

$(function() {
	$('select').each(function() {
		$(this).select2({
			theme: 'bootstrap4',
			width: '100%',
			placeholder: $(this).attr('data-placeholder'),
			allowClear: Boolean($(this).data('allow-clear')),
		});
	});
});

$('#search-within-org').on("select2:selecting", function(e) {
	// what you would like to happen when making a selection
	set_selected_org = function() {
		var selected_org = $('.select2-selection__rendered').html();
		$('.filtered-search-within-org').addClass('filter-changed');
		$('.filtered-search-within-org').html(selected_org);
	};
	setTimeout(set_selected_org, 100);
	setTimeout(function() {
		$(".filtered-search-within-org").toggleClass('alert_here')
	}, 500);
	setTimeout(function() {
		$(".filtered-search-within-org").toggleClass('alert_here')
	}, 1000);
});

$('.filtered-search-within-org').click(function() {
	$(this).replaceWith($("<span class=\"filtered-search-within-org\">hela Sverige</span>"));
});

// select2.org - stop

// Stop default link click behavior for nav-links
$('.mock-link, .pointer-link>.toggle-sidebar, .obj_name').click(function(e) {
	e.stopPropagation();
	e.preventDefault();
});
