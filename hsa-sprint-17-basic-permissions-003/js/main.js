// Prototyping START

var pathArray = window.location.pathname.split('/');
var firstLevelLocation = pathArray[1];
// console.log(firstLevelLocation);

var prototype_message;
$.get("/" + firstLevelLocation + "/assets/prototype-message.html", function(response) {
	// prototype_message contains whatever that request returned
	prototype_message = response;
	$("body#page-simple-search").prepend(prototype_message);
	$("#prototype-message-placeholder").click(function() {
		$(this).remove();
	});
	// Remove if not hsa-sprint-16-admin-004
	// $("#prototype-message-placeholder .inner")
	// 	.append('<br><a href="' + '/' + firstLevelLocation + '/locality/blekinge-lan/region-blekinge/blekingesjukhuset/units/' + '" style="font-size:.75em">"detaljvy" för administratör</a>')
	// 	.append('<br><a href="' + '/' + firstLevelLocation + '/locality/blekinge-lan/region-blekinge/blekingesjukhuset/units/user-without-extended-permissions.html' + '" style="font-size:.75em">"detaljvy" inloggad utan adminbehörighet</a>');
});

$(function() {
	setTimeout(function() {
		$('#prototype-message-placeholder').toggleClass('fade-2-visible fade-2-hidden');
	}, 5000);
});

/* Create a popover for mock-links */

var mock_message_popover_options = {
	// template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"><h4>Fungerar inte i prototyp</h4><p>Kommer att fungera i skarp version!</p></div></div>',
	content: '<h4>Fungerar inte i prototyp</h4><p>Kommer att fungera i skarp version!</p>',
	html: true,
	placement: 'auto'
};

$('.mock-message').popover(mock_message_popover_options);

/* Auto hide timer for prototype popover */

$('.mock-message').click(function(e) {
	setTimeout(function() {
		$('[data-original-title]').popover('hide').removeClass("mock-message-popover");
	}, 5000);
});

// Enable styling for protootype messages
$(".mock-link").click(function() {
	$('.popover.show').removeClass("mock-message-popover").addClass("mock-message-popover");
});

// Simulate 200+ hits limit START
$(function() {
	$('.omnibox').on('input', "#omniboxSearch01", function() {
		if ($(this).val().length && $(this).val() === 'anna') {
			// alert('YES, ANNA IT IS!');
			$('form#hsa-simple-search').attr('action', function(i, v) {
				return v.replace(/results\.html/g, 'results-200-limit\.html');
			});
		}
	});
});
// Simulate 200+ hits limit STOP

$('#omniboxSearch01').blur(function() {
	if ($(this).val() == '') {
		console.log('EMPTY SEARCH!');
		$('form').attr('action', function(i, v) {
			return v.replace(/results\.html/g, 'results-empty\.html');
		});
	}
});

var getUrlParameter = function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
		}
	}
};

var searchString = getUrlParameter('omniboxSearch01');
// console.log(searchString);
$("#omniboxSearch01").attr("placeholder", searchString);
$("#search-term-in-result-view").html(searchString);

// Prototyping STOP

// popover setting START

/* Where to show the message? */

$(function() {
	$('.general-message').popover({
		container: 'body'
	})
})

/* Dismiss all popovers by clicking outside */

$('html').on('click', function(e) {
	if (typeof $(e.target).data('original-title') == 'undefined') {
		$('[data-original-title]').popover('hide');
	}
});

/* Auto hide timer for general-message popover */

$('.general-message').click(function(e) {
	setTimeout(function() {
		$('[data-original-title]').popover('hide');
	}, 5000);
});

// popover setting STOP

// Add onblur & onfocus to all SEARCH form fields... START

function onBlur(el) {
	if (el.value == '') {
		el.value = el.defaultValue;
	}
}

function onFocus(el) {
	if (el.value == el.defaultValue) {
		el.value = '';
	}
}

// $(function() {
// 	$('input').attr('onblur', 'onBlur(this)').attr('onfocus', 'onFocus(this)');
// });

// Add onblur & onfocus to all SEARCH form fields... STOP

// Detailed search - toggle disabled fields START

// Start simple, enable all fields

// $('input[id^=searchCategory]').click(function () {
//   $('input[id^=searchField]').prop('disabled', false);
// });

// Toggle disable
// Person
$('#searchCategory1').click(function() {
	$('#searchField01,#searchField03,#searchField05').prop('disabled', function(i, v) {
		return !v;
	});
	// $('#searchField08').next().toggleClass('disabled');
});

// Unit
$('#searchCategory2').click(function() {
	$('#searchField08').prop('disabled', function(i, v) {
		return !v;
	});
	$('#searchField08').next().toggleClass('disabled');
});

// Commission
$('#searchCategory3').click(function() {
	$('#searchField02,#searchField04,#searchField10').prop('disabled', function(i, v) {
		return !v;
	});
	$('#searchField10').next().toggleClass('disabled');
});

// Detailed search - toggle disabled fields STOP

$(function() {
	if ($(".selectpicker").length) {
		$(".selectpicker").selectpicker({
			noneSelectedText: 'Välj något' // by this default 'Nothing selected' --> will change to whatever you enter here
		});
	}
});

$('.toggle-sidebar').on('click', function() {
	$('#sidebar-left').toggleClass('active');
	$('.pointer-link>.toggle-sidebar').toggleClass('opened closed');
});
$('.toggle-filters').on('click', function() {
	$('#filter-submenu').toggleClass('show');
	$('#left-nav-item-01>.dropdown-toggle').attr('aria-expanded', 'true');
});

// Treeview - START

$('#expand-all-branches').on('click', function(e) {
	$('ul.tree-view i.fa-caret-right').removeClass('fa fa-caret-right').addClass('fa fa-chevron-down');
	$('ul.tree-view ul').show();
	$('#collapse-all-branches').removeClass('d-none').addClass('d-block');
	$(this).removeClass('d-block').addClass('d-none');
	return false;
});

$('#collapse-all-branches').on('click', function(e) {
	$('ul.tree-view i.fa-chevron-down').removeClass('fa fa-chevron-down').addClass('fa fa-caret-right');
	$('ul.tree-view ul').hide();
	$('#expand-all-branches').removeClass('d-none').addClass('d-block');
	$(this).removeClass('d-block').addClass('d-none');
	return false;
});

// $(document).on('show.bs.modal', '#exampleModalCenter', function () {
//   alert('#exampleModalCenter');
//   //Do stuff here
// });

$('ul.tree-view li').on('click', function(e) {
	if ($(this).has("ul").length) {
		if ($(this).find('ul:first').is(":visible")) {
			$(this).find('i:first').removeClass('fa fa-chevron-down').addClass('fa fa-caret-right');
			$(this).find('ul:first').hide();
			e.stopPropagation();
		} else {
			$(this).find('ul:first').show();
			$(this).find('i:first').removeClass('fa fa-caret-right').addClass('fa fa-chevron-down');
			e.stopPropagation();
		}
	} else {
		e.stopPropagation();
	}
});

$('ul.tree-view li a').on('click', function(e) {
	$('ul.tree-view li a.selectedDoc').removeClass('selectedDoc');
	$(this).addClass('selectedDoc');
	// get Doc ID
	var thisDocID = $(this).attr("data-docid");
	alert("clicked Document " + thisDocID);
	return false;
});

// Treeview - STOP

// extend browse view - START

this.addClasses = function(parent, level) {
	// add a class to all <li> elements on this level:
	parent.children("li").addClass("cat-level-" + level);
	// find any child <ul> elements and run this function on them,
	// making sure to increment the level counter:
	if (parent.children("li").children("ul").length) {
		this.addClasses(parent.children("li").children("ul"), level + 1);
	}
};
// start off by selecting only the top-level <ul> elements:
this.addClasses($('ul.dynamic-headings'), 1);

$('ul.tree-view li').filter(function() {
	if ($(this).has("ul").length) {
		$(this).addClass('has-children');
	} else {
		$(this).addClass('has-no-child');
	}
});

// change top-level <li> into heading:
$('li.cat-level-1').contents().filter(function() {
	return this.nodeType === 3 && $.trim(this.nodeValue).length;
}).wrap('<h3 class="obj_name" />');

// hide detail-view for now:
// $('.detail-view').children().addClass('d-none');

// Detail view - START

$('#page-detail-view.preview .card .card-body:not(:has(".card-text"))').parent().hide();

$('#page-detail-view .card .card-body:not(:has(".card-text")')
	.children().css("border-bottom", "1px solid #e9ecef")
$(".card-text").css("border-bottom", "none");

// $('#page-detail-view .card .card-body:not(:has(".card-text"))').css("border", "3px solid red");
$('#page-detail-view .card .card-body:not(:has(".card-text"))')
	.parent().addClass("border-danger")
	.children().prepend('<a href="#" class="mock-link mock-message"><i class="fa fa-pen float-right text-black-50"></i></a>');

// Detail view - STOP


$('li.obj_o').contents().filter(function() {
	return this.nodeType === 3 && $.trim(this.nodeValue).length;
}).wrap('<span class="obj_name" />');

$('li.has-no-child>.obj_name').wrap('<a href="#" />');

$("#page-browse .obj_name").after('<span class="number-of-objects num-gen"></span><span class="attributes-quick-view text-nowrap"><i class="fa fa-file-alt"></i>visa</span>')

$("ul.tree-view .attributes-quick-view").click(function(e) {
	$('#short-view-modal').modal('show')
	e.stopPropagation(); // This prevents branch to expand
});

function getNumber() {
	$('.num-gen').each(function() {
		var minNumber = 5;
		var maxNumber = 200;
		var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
		//$(this).html("<b>" + randomnumber + "</b> objekt");
		$(this).html(randomnumber);
	});
}

getNumber();

$("ul.tree-view li:has(> ul)").prepend('<i class="fa fa-caret-right"></i>');

// extend browse view - STOP

// select2.org - START

$(function() {
	$('select').each(function() {
		$(this).select2({
			language: "sv",
			// theme: 'bootstrap4',
			width: '100%',
			placeholder: $(this).attr('data-placeholder'),
			allowClear: Boolean($(this).data('allow-clear')),
		});
	});
});

// Filtrera sökområde = searchable_locactions – START
var searchable_locactions = [];
$.getJSON("/" + firstLevelLocation + "/assets/search-area-medium-190209.json", function(data) {
	//parse json
	searchable_locactions = data;
	// console.log(searchable_locactions);
});

$('#search-within-org').select2({
	data: {
		results: searchable_locactions,
		text: 'name'
	},
	placeholder: $(this).attr('data-placeholder'),
	allowClear: Boolean($(this).data('allow-clear')),

});

$(document).ajaxStop(function() {
	var options = $('#search-within-org');
	$.each(searchable_locactions, function() {
		options.append($("<option />").text(this.name));
	});
});

// Filtrera sökområde = searchable_locactions – STOP

$('#search-within-org').on("select2:selecting", function(e) {
	// what you would like to happen when making a selection
	set_selected_org = function() {
		var selected_org = $('.select2-selection__rendered').html();
		$('.filtered-search-within-org').addClass('filter-changed');
		$('.filtered-search-within-org').html(selected_org);
	};
	setTimeout(set_selected_org, 100);
	setTimeout(function() {
		$(".filtered-search-within-org").toggleClass('alert_here')
	}, 500);
	setTimeout(function() {
		$(".filtered-search-within-org").toggleClass('alert_here')
	}, 1000);
});

$('.filtered-search-within-org').click(function() {
	$(this).replaceWith($("<span class=\"filtered-search-within-org\">hela Sverige</span>"));
});

// select2.org - STOP

// Stop default link click behavior for nav-links
$('.mock-link, .pointer-link>.toggle-sidebar, .obj_name').click(function(e) {
	e.stopPropagation();
	e.preventDefault();
});

// Cookie message – START

if (!!$.cookie('acceptCookies')) {
  // have cookie
  // alert('I collect cookies!');
  // $('body').removeClass('has-cookie-alert');
} else {
  // no cookie
  // alert('I am concidering accepting cookies but still not sure.');
  $('body').addClass('has-cookie-alert');
}
