// Prototyping START

// Stop default link click behavior for nav-links
$('.mock-link, .pointer-link>.sidebarLeftCollapse').click(function (e) {
	e.stopPropagation();
	e.preventDefault();
});

/* Create a popover for mock-links */

var popover_options = {
	content: '<h4>Fungerar inte i prototyp</h4><p>Kommer att fungera i skarp version!</p>',
	html: true,
	placement: 'auto'
};

$('.mock-message').popover(popover_options);

/* Auto hide timer for popover */

$('.mock-message').click(function(e) {
	setTimeout(function() {
		$('[data-original-title]').popover('hide');
	}, 6000);
});

/* Dismiss all popovers by clicking outside */

$('html').on('click', function(e) {
	if (typeof $(e.target).data('original-title') == 'undefined') {
		$('[data-original-title]').popover('hide');
	}
});

$("a.mock-link").click(function() {
	// remove classes from all
	$("a.mock-link").removeClass("active");
	// add class to the one we clicked
	$(this).addClass("active");
});

// Prototyping STOP

function onBlur(el) {
	if (el.value == '') {
		el.value = el.defaultValue;
	}
}

function onFocus(el) {
	if (el.value == el.defaultValue) {
		el.value = '';
	}
}

// Add onblur & onfocus to all form fields...
$(function() {
	$('input').attr('onblur', 'onBlur(this)').attr('onfocus', 'onFocus(this)');
});

// Detailed search - toggle disabled fields - START

// Start simple, enable all fields

// $('input[id^=searchCategory]').click(function () {
//   $('input[id^=searchField]').prop('disabled', false);
// });

// Person
// Toggle disable
$('#searchCategory1').click(function() {
	$('#searchField02,#searchField10').prop('disabled', function(i, v) {
		return !v;
	});
});

// Unit
$('#searchCategory2').click(function() {
	$('#searchField01,#searchField02,#searchField03').prop('disabled', function(i, v) {
		return !v;
	});
});

// Locality
$('#searchCategory3').click(function() {
	$('#searchField02,#searchField08,#searchField10').prop('disabled', function(i, v) {
		return !v;
	});
});

// Detailed search - toggle disabled fields - STOP

// Sidebar Navigation toggle - start

$(".toggle-sidebar").click(function() {
	// var oldArrSrc = './images/push-menu-left.png';
	// var newArrSrc = './images/push-menu-right.png';
	if ($('#sidebar-old>.sidebar-sticky:visible').length) {
		$('#sidebar-old>.sidebar-sticky').addClass("d-md-none");
		$('.pointer-link>.toggle-sidebar').removeClass("opened").addClass("closed");
		$('.nav-link.toggle-sidebar').removeClass("active");
		$('#sidebar-old').removeClass("col-sm-2 col-md-2");
		$('.sidebar-border').removeClass("align-right").addClass("align-left");
		$('#main-content').removeClass("offset-md-3 col-md-8").addClass("offset-md-2 col-md-8 mx-md-auto");
		$('.footer').removeClass("offset-md-3 col-sm-12 col-md-8").addClass("offset-md-2 col-md-8");
		// $('img.toggle-sidebar[src="' + oldArrSrc + '"]').attr('src', newArrSrc);
		console.log("sidebar hidden");
	} else {
		$('.pointer-link>.toggle-sidebar').removeClass("closed").addClass("opened");
		$('.nav-link.toggle-sidebar').addClass("active");
		$('#sidebar-old').addClass("col-sm-2 col-md-2");
		$('.sidebar-border').removeClass("align-left").addClass("align-right");
		$('#sidebar-old>.sidebar-sticky').removeClass("d-md-none");
		$('#main-content').removeClass("offset-md-2 col-md-8 mx-md-auto").addClass("offset-md-3 col-md-8");
		$('.footer').removeClass("offset-md-2 col-md-8").addClass("offset-md-3 col-sm-12 col-md-8");
		console.log("sidebar shown");
	}
});

$(function () {
  $('.sidebarLeftCollapse').on('click', function () {
    $('#sidebar-left').toggleClass('active');
  });
});
