(function (t) {
    function a(a) {
        for (var s, r, o = a[0], l = a[1], c = a[2], d = 0, h = []; d < o.length; d++) r = o[d], n[r] && h.push(n[r][0]), n[r] = 0;
        for (s in l) Object.prototype.hasOwnProperty.call(l, s) && (t[s] = l[s]);
        u && u(a);
        while (h.length) h.shift()();
        return i.push.apply(i, c || []), e()
    }

    function e() {
        for (var t, a = 0; a < i.length; a++) {
            for (var e = i[a], s = !0, o = 1; o < e.length; o++) {
                var l = e[o];
                0 !== n[l] && (s = !1)
            }
            s && (i.splice(a--, 1), t = r(r.s = e[0]))
        }
        return t
    }
    var s = {},
        n = {
            app: 0
        },
        i = [];

    function r(a) {
        if (s[a]) return s[a].exports;
        var e = s[a] = {
            i: a,
            l: !1,
            exports: {}
        };
        return t[a].call(e.exports, e, e.exports, r), e.l = !0, e.exports
    }
    r.m = t, r.c = s, r.d = function (t, a, e) {
        r.o(t, a) || Object.defineProperty(t, a, {
            enumerable: !0,
            get: e
        })
    }, r.r = function (t) {
        "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(t, "__esModule", {
            value: !0
        })
    }, r.t = function (t, a) {
        if (1 & a && (t = r(t)), 8 & a) return t;
        if (4 & a && "object" === typeof t && t && t.__esModule) return t;
        var e = Object.create(null);
        if (r.r(e), Object.defineProperty(e, "default", {
                enumerable: !0,
                value: t
            }), 2 & a && "string" != typeof t)
            for (var s in t) r.d(e, s, function (a) {
                return t[a]
            }.bind(null, s));
        return e
    }, r.n = function (t) {
        var a = t && t.__esModule ? function () {
            return t["default"]
        } : function () {
            return t
        };
        return r.d(a, "a", a), a
    }, r.o = function (t, a) {
        return Object.prototype.hasOwnProperty.call(t, a)
    }, r.p = "/";
    var o = window["webpackJsonp"] = window["webpackJsonp"] || [],
        l = o.push.bind(o);
    o.push = a, o = o.slice();
    for (var c = 0; c < o.length; c++) a(o[c]);
    var u = l;
    i.push([0, "chunk-vendors"]), e()
})({
    0: function (t, a, e) {
        t.exports = e("56d7")
    },
    "034f": function (t, a, e) {
        "use strict";
        var s = e("64a9"),
            n = e.n(s);
        n.a
    },
    "0836": function (t, a, e) {
        "use strict";
        var s = e("b738"),
            n = e.n(s);
        n.a
    },
    "0abd": function (t, a, e) {
        "use strict";
        var s = e("dadb"),
            n = e.n(s);
        n.a
    },
    1: function (t, a) {},
    "15ee": function (t, a, e) {
        t.exports = e.p + "../ui-search--dist--181211/img/hsa-logo--89x39px.c9a015c8.svg"
    },
    "56d7": function (t, a, e) {
        "use strict";
        e.r(a);
        e("cadf"), e("551c"), e("097d");
        var s = e("2b0e"),
            n = function () {
                var t = this,
                    a = t.$createElement,
                    s = t._self._c || a;
                return s("div", {
                    attrs: {
                        id: "app"
                    }
                }, [s("nav", {
                    staticClass: "navbar navbar-expand-xl fixed-top navbar-dark hsa-bg-dark pr-4 shadow"
                }, [t._m(0), t._m(1), !1 !== t.showNav ? s("div", {
                    staticClass: "collapse navbar-collapse",
                    attrs: {
                        id: "collapsibleNavBar",
                        role: "navigation"
                    }
                }, [s("ul", {
                    staticClass: "navbar-nav mr-auto"
                }, [s("li", {
                    staticClass: "nav-item"
                }, [s("a", {
                    staticClass: "nav-link active",
                    attrs: {
                        href: "#/table"
                    }
                }, [t._v(t._s(t.$t("search")))])]), s("li", {
                    staticClass: "nav-item"
                }, [s("a", {
                    staticClass: "nav-link",
                    attrs: {
                        href: "#/table"
                    }
                }, [t._v(t._s(t.$t("browse")))])]), s("li", {
                    staticClass: "nav-item"
                }, [s("a", {
                    staticClass: "nav-link pointer-link",
                    on: {
                        click: t.toggleSidebar
                    }
                }, [t._v(t._s(t.$t("administrate")))])]), s("li", {
                    staticClass: "nav-item dropdown"
                }, [s("a", {
                    staticClass: "nav-link dropdown-toggle",
                    attrs: {
                        href: "#",
                        id: "dropdown06",
                        "data-toggle": "dropdown",
                        "aria-haspopup": "true",
                        "aria-expanded": "false"
                    }
                }, [t._v(t._s(t.$t("language")))]), s("div", {
                    staticClass: "dropdown-menu",
                    attrs: {
                        "aria-labelledby": "dropdown06"
                    }
                }, [s("a", {
                    staticClass: "dropdown-item",
                    attrs: {
                        href: "#"
                    },
                    on: {
                        click: function (a) {
                            a.preventDefault(), t.setLocale("sv")
                        }
                    }
                }, [t._v("SV")]), s("a", {
                    staticClass: "dropdown-item",
                    attrs: {
                        href: "#"
                    },
                    on: {
                        click: function (a) {
                            a.preventDefault(), t.setLocale("en")
                        }
                    }
                }, [t._v("EN")]), s("a", {
                    staticClass: "dropdown-item",
                    attrs: {
                        href: "#"
                    }
                }, [t._v("Arabiska")])])])]), s("ul", {
                    staticClass: "navbar-nav"
                }, [s("li", {
                    staticClass: "nav-item"
                }, [s("a", {
                    staticClass: "nav-link text-nowrap",
                    attrs: {
                        href: "./"
                    }
                }, [s("i", {
                    staticClass: "fa fa-user pr-2"
                }), t._v("\n            " + t._s(t.$t("logout")) + "\n          ")])])])]) : t._e()]), s("div", {
                    staticClass: "container-fluid"
                }, [s("div", {
                    staticClass: "main-content-full-height",
                    class: {
                        "sidebar-offset sidebar-expanded-main-width": t.showMenu
                    }
                }, [s("main", {
                    staticClass: "col-lg-12 ml-sm-auto px-4",
                    attrs: {
                        id: "main-content"
                    }
                }, [s("router-view")], 1), t.showMenu ? s("div", {
                    staticClass: "left-nav"
                }, [t._m(2)]) : t._e()]), s("div", {
                    staticClass: "abs-left d-flex vh-100 p-1 hsa-dcd9d6",
                    class: {
                        "sidebar-offset": t.showMenu
                    }
                }), s("div", {
                    staticClass: "abs-left d-flex vh-100"
                }, [s("nav", {
                    staticClass: "justify-content-center align-self-center pointer-link"
                }, [t.showMenu ? t._e() : s("img", {
                    staticClass: "toggle-sidebar",
                    attrs: {
                        src: e("bad0"),
                        alt: "expand menu"
                    },
                    on: {
                        click: t.toggleSidebar
                    }
                }), t.showMenu ? s("img", {
                    staticClass: "toggle-sidebar sidebar-offset",
                    attrs: {
                        src: e("8489"),
                        alt: "collapse menu"
                    },
                    on: {
                        click: t.toggleSidebar
                    }
                }) : t._e()])])]), s("hsa-footer")], 1)
            },
            i = [function () {
                var t = this,
                    a = t.$createElement,
                    s = t._self._c || a;
                return s("a", {
                    staticClass: "navbar-brand col-2 mr-0 pt-3 pl-4",
                    attrs: {
                        href: "./"
                    }
                }, [s("img", {
                    attrs: {
                        src: e("15ee"),
                        alt: "HSA"
                    }
                })])
            }, function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("button", {
                    staticClass: "navbar-toggler",
                    attrs: {
                        type: "button",
                        "data-toggle": "collapse",
                        "data-target": "#collapsibleNavBar",
                        "aria-controls": "collapsibleNavBar",
                        "aria-expanded": "false",
                        "aria-label": "Toggle Menu"
                    }
                }, [e("span", {
                    staticClass: "navbar-toggler-icon"
                })])
            }, function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("nav", {
                    staticClass: "sidebar-width d-none d-md-block hsa-bg-light",
                    attrs: {
                        id: "left-sidebar"
                    }
                }, [e("ul", {
                    staticClass: "list-unstyled components"
                }, [e("li", {
                    staticClass: "active"
                }, [e("a", {
                    staticClass: "dropdown-toggle",
                    attrs: {
                        href: "#homeSubmenu",
                        "data-toggle": "collapse",
                        "aria-expanded": "false"
                    }
                }, [t._v("Home")]), e("ul", {
                    staticClass: "collapse list-unstyled",
                    attrs: {
                        id: "homeSubmenu"
                    }
                }, [e("li", [e("a", {
                    attrs: {
                        href: "#"
                    }
                }, [t._v("Home 1")])]), e("li", [e("a", {
                    attrs: {
                        href: "#"
                    }
                }, [t._v("Home 2")])]), e("li", [e("a", {
                    attrs: {
                        href: "#"
                    }
                }, [t._v("Home 3")])])])]), e("li", [e("a", {
                    attrs: {
                        href: "#"
                    }
                }, [t._v("About")])]), e("li", [e("a", {
                    staticClass: "dropdown-toggle",
                    attrs: {
                        href: "#page-submenu",
                        "data-toggle": "collapse",
                        "aria-expanded": "false"
                    }
                }, [t._v("Pages")]), e("ul", {
                    staticClass: "collapse list-unstyled",
                    attrs: {
                        id: "page-submenu"
                    }
                }, [e("li", [e("a", {
                    attrs: {
                        href: "#"
                    }
                }, [t._v("Page 1")])]), e("li", [e("a", {
                    attrs: {
                        href: "#"
                    }
                }, [t._v("Page 2")])]), e("li", [e("a", {
                    attrs: {
                        href: "#"
                    }
                }, [t._v("Page 3")])])])]), e("li", [e("a", {
                    attrs: {
                        href: "#"
                    }
                }, [t._v("Portfolio")])]), e("li", [e("a", {
                    attrs: {
                        href: "#"
                    }
                }, [t._v("Contact")])])])])
            }],
            r = function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("footer", {
                    staticClass: "footer container-fluid text-muted col-sm-12"
                }, [e("hr", {
                    staticClass: "mx-3"
                }), e("div", {
                    staticClass: "container-fluid row"
                }, [t._m(0), e("div", {
                    staticClass: "footer-global-content col col-12 col-sm-4"
                }, [e("h2", {
                    staticClass: "heading-small"
                }, [t._v(t._s(t.$t("moreAboutHsa")))]), t._m(1)]), e("div", {
                    staticClass: "footer-global-content col col-12 col-sm-4"
                }, [e("h2", {
                    staticClass: "heading-small"
                }, [t._v(t._s(t.$t("help")) + ", " + t._s(t.$t("support")) + ", " + t._s(t.$t("questions")))]), e("p", {
                    staticClass: "fine"
                }, [e("span", [t._v(t._s(t.$t("contactYourLocalHSaAdminOrHsaAgent")))])])])])])
            },
            o = [function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("div", {
                    staticClass: "footer-global-logo col col-12 col-sm-4"
                }, [e("a", {
                    staticClass: "logo",
                    attrs: {
                        href: "https://www.inera.se/"
                    }
                }, [e("div", {
                    staticClass: "logo-img"
                }, [t._v("Inera")])])])
            }, function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("p", {
                    staticClass: "fine"
                }, [e("span", [e("a", {
                    attrs: {
                        href: "https://www.inera.se/hsa"
                    }
                }, [t._v("www.inera.se/hsa")])])])
            }],
            l = {
                name: "hsaFooter",
                data: function () {
                    return {
                        showFooter: !0
                    }
                },
                computed: {}
            },
            c = l,
            u = (e("0836"), e("2877")),
            d = Object(u["a"])(c, r, o, !1, null, null, null);
        d.options.__file = "footer.vue";
        var h = d.exports,
            p = e("a925");
        s["a"].use(p["a"]);
        var f = new p["a"]({
            locale: "sv",
            messages: {
                sv: {
                    search: "Sök",
                    browse: "Bläddra",
                    administrate: "administrera",
                    logIn: "logga in",
                    logout: "logga ut",
                    unit: "enhet",
                    searchResult: "Sök resultat",
                    searchHere: "Sök här",
                    moreAboutHsa: "Mer om HSA",
                    help: "Help",
                    support: "Support",
                    questions: "Frågor",
                    language: "Språk",
                    contactYourLocalHSaAdminOrHsaAgent: "Kontakta din lokala HSA-förvaltning eller ditt HSA-ombud",
                    readMoreAndFilter: "Läs mer och filtrera",
                    persons: "Personer",
                    units: "Enheter"
                },
                en: {
                    search: "Search",
                    browse: "Browse",
                    administrate: "Administer",
                    logIn: "login",
                    logout: "logout",
                    unit: "unit",
                    searchResult: "Search result",
                    searchHere: "Search here",
                    moreAboutHsa: "More about Hsa",
                    help: "Help",
                    support: "Support",
                    questions: "Questions",
                    language: "Language",
                    contactYourLocalHSaAdminOrHsaAgent: "Contact your local HSA-administration or your HSA-Agent",
                    readMoreAndFilter: "Read more and filter",
                    persons: "Persons",
                    units: "Units"
                }
            }
        });
        f.locale = null === localStorage.getItem("locale") ? "sv" : localStorage.getItem("locale");
        var v = {
            setLocale: g
        };

        function g(t) {
            localStorage.setItem("locale", t), console.log(localStorage.getItem("locale")), f.locale = t
        }
        s["a"].component("hsa-footer", h), s["a"].use(v);
        var m = {
                name: "app",
                i18n: f,
                UiTextService: v,
                showMenu: !1,
                showNav: "",
                showFilter: !1,
                className: "toggle-sidebar",
                display: "block",
                expandedRowClass: "darker",
                windowWidth: 0,
                methods: {
                    setLocale: function (t) {
                        v.setLocale(t), this.$router.go(this.$router.currentRoute)
                    },
                    toggleSidebar: function () {
                        console.log("Toggling sidebar"), this.showMenu ? this.className = "toggle-sidebar d-block" : this.className = "toggle-sidebar d-none", this.showMenu = !this.showMenu
                    },
                    toggleFilter: function () {
                        this.showFilter = !this.showFilter, this.showFilter ? this.expandedRowClass = "brighter" : this.expandedRowClass = "darker"
                    },
                    toggleNav: function () {
                        this.showNav = !this.showNav, console.log("inside toggle nav")
                    }
                },
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        window.addEventListener("resize", function () {
                            t.windowWidth = window.innerWidth
                        })
                    })
                },
                watch: {
                    windowWidth: function (t, a) {
                        t < 750 && (this.showMenu = !1)
                    }
                },
                data: function () {
                    return {
                        showMenu: !1,
                        showNav: "",
                        className: "toggle-sidebar",
                        display: "showSideBar",
                        showFilter: !1,
                        expandedRowClass: "darker",
                        windowWidth: window.innerWidth
                    }
                }
            },
            b = m,
            _ = (e("034f"), Object(u["a"])(b, n, i, !1, null, null, null));
        _.options.__file = "App.vue";
        var w = _.exports,
            C = e("8c4f"),
            A = function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("div", {
                    staticClass: "row justify-content-md-center container-fluid pt-5"
                }, [e("div", {
                    staticClass: "col-md-6"
                }, [e("div", {
                    staticClass: "input-group"
                }, [e("input", {
                    directives: [{
                        name: "model",
                        rawName: "v-model",
                        value: t.searchInput,
                        expression: "searchInput"
                    }],
                    staticClass: "form-control border-secondary py-4 px-4 mr-3",
                    attrs: {
                        type: "search",
                        size: "40",
                        value: "search",
                        placeholder: "Sök person eller enhet (namn, titel, ort, HSA-id m.m.)"
                    },
                    domProps: {
                        value: t.searchInput
                    },
                    on: {
                        change: function (a) {
                            t.getOrganizations()
                        },
                        input: function (a) {
                            a.target.composing || (t.searchInput = a.target.value)
                        }
                    }
                }), e("button", {
                    staticClass: "btn btn-outline-secondary local_button px-4",
                    attrs: {
                        type: "button"
                    },
                    on: {
                        click: t.getOrganizations
                    }
                }, [e("i", {
                    staticClass: "fa fa-search pr-2"
                }), t._v("\n        " + t._s(t.$t("search")) + "\n      ")])])]), e("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: "" != t.searchInput && (t.units.length > 0 || t.persons.length > 0 || t.organizations.length > 0),
                        expression: "searchInput !='' && ( units.length > 0 || persons.length > 0 || organizations.length > 0 )"
                    }],
                    staticClass: "row col-12 pt-5 container-fluid"
                }, [e("div", {
                    staticClass: "col-md-8"
                }, [t._v(t._s(t.searchInput) + " gav " + t._s(t.units.length + t.persons.length + t.organizations.length) + " träffar varav " + t._s(t.units.length + t.persons.length + t.organizations.length) + " visas")]), e("div", {
                    staticClass: "col-md-4"
                }, [e("a", {
                    attrs: {
                        href: "#"
                    },
                    on: {
                        click: t.showReadMore
                    }
                }, [t.readMore ? t._e() : e("i", {
                    staticClass: "fa fa-angle-right"
                }), t.readMore ? e("i", {
                    staticClass: "fa fa-angle-down"
                }) : t._e(), t._v("\n        " + t._s(t.$t("readMoreAndFilter")) + "\n      ")])])]), e("div", {
                    staticClass: "container-fluid mt-4 justify-content-md-center"
                }, [e("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.units.length < 1 && t.persons.length < 1 && t.organizations.length < 1 && "" != t.searchInput,
                        expression: "units.length < 1 && persons.length < 1 && organizations.length < 1 && searchInput != ''"
                    }],
                    staticClass: "alert alert-warning offset-md-3 col-md-6",
                    attrs: {
                        role: "alert"
                    }
                }, [t._v("\n      " + t._s(t.searchInput) + " gav inga träff! Försök igen.\n      "), t._m(0)])]), t.readMore ? e("div", [e("li", [t._v("OBS! Resultlistat har kortats ner. Du kan bara se upp till 200 träffar")]), e("li", [t._v("OBS! Resultlistat har kortats ner. Du kan bara se upp till 200 träffar")]), e("li", [t._v("OBS! Resultlistat har kortats ner. Du kan bara se upp till 200 träffar")]), e("li", [t._v("OBS! Resultlistat har kortats ner. Du kan bara se upp till 200 träffar")])]) : t._e(), e("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.units.length > 0 || t.persons.length > 0 || t.organizations.length > 0,
                        expression: "( units.length > 0 || persons.length > 0 || organizations.length > 0 )"
                    }],
                    staticClass: "tab-container",
                    attrs: {
                        id: "tabs"
                    }
                }, [e("div", {
                    staticClass: "tabs"
                }, [e("div", [e("a", {
                    class: [1 === t.activetab ? "active" : ""],
                    on: {
                        click: function (a) {
                            t.activetab = 1
                        }
                    }
                }, [e("i", {
                    staticClass: "fa fa-user-circle circle-icon"
                }), e("strong", [t._v("Personer")]), t._v("\n          (" + t._s(t.persons.length) + ")\n        ")])]), e("div", [e("a", {
                    class: [2 === t.activetab ? "active" : ""],
                    on: {
                        click: function (a) {
                            t.activetab = 2
                        }
                    }
                }, [e("i", {
                    staticClass: "fa fa-home circle-icon"
                }), e("strong", [t._v("Enhet")]), t._v("\n          (" + t._s(t.units.length) + ")\n          "), e("i", {
                    staticClass: "fa fa-angle-right div-only-mobile"
                })])]), e("div", [e("a", {
                    class: [3 === t.activetab ? "active" : ""],
                    on: {
                        click: function (a) {
                            t.activetab = 3
                        }
                    }
                }, [e("i", {
                    staticClass: "fa fa-map-marker circle-icon"
                }), t._v(" Medarbetareuppdrag (0)\n        ")])]), e("div", [e("a", {
                    class: [4 === t.activetab ? "active" : ""],
                    on: {
                        click: function (a) {
                            t.activetab = 4
                        }
                    }
                }, [e("i", {
                    staticClass: "fa fa-sitemap"
                }), t._v("\n          Organizationer (" + t._s(t.organizations.length) + ")\n        ")])])]), e("div", {
                    staticClass: "content"
                }, [1 === t.activetab ? e("div", {
                    staticClass: "tabcontent"
                }, [e("div", {
                    staticClass: "col-md-12 shadow"
                }, t._l(t.persons, function (a, s) {
                    return e("div", {
                        key: a.link,
                        staticClass: "row",
                        attrs: {
                            i: ++s
                        }
                    }, [e("div", {
                        staticClass: "col-md-9"
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [e("a", {
                        attrs: {
                            href: a.link
                        }
                    }, [e("strong", [t._v(t._s(a.name))])]), e("i", {
                        staticClass: "fa fa-envelope p-2"
                    })]), e("p", [t._v(t._s(t.formatPath(a.path)))])]), e("div", {
                        staticClass: "col-md-3"
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [t._v(t._s(a.type))])]), e("hr", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: s < t.persons.length,
                            expression: "i < persons.length"
                        }]
                    })])
                }))]) : t._e(), 2 === t.activetab ? e("div", {
                    staticClass: "tabcontent"
                }, [e("div", {
                    staticClass: "col-md-12 shadow"
                }, t._l(t.units, function (a, s) {
                    return e("div", {
                        key: a.link,
                        staticClass: "row",
                        attrs: {
                            i: ++s
                        }
                    }, [e("div", {
                        staticClass: "col-md-9"
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [e("a", {
                        attrs: {
                            href: a.link
                        }
                    }, [e("strong", [t._v(t._s(a.name))])])]), e("p", [t._v(t._s(t.formatPath(a.path)))])]), e("div", {
                        staticClass: "col-md-3"
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [t._v("Enhet")])])])
                }))]) : t._e(), 3 === t.activetab ? e("div", {
                    staticClass: "tabcontent"
                }, [t._v("Medarbetareuppdrag")]) : t._e(), 4 === t.activetab ? e("div", {
                    staticClass: "tabcontent"
                }, [e("div", {
                    staticClass: "col-md-12 shadow"
                }, t._l(t.organizations, function (a, s) {
                    return e("div", {
                        key: a.link,
                        staticClass: "row",
                        attrs: {
                            i: ++s
                        }
                    }, [e("div", {
                        staticClass: "col-md-9"
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [e("a", {
                        attrs: {
                            href: a.link
                        }
                    }, [e("strong", [t._v(t._s(a.name))])])]), e("p", [t._v(t._s(t.formatPath(a.path)))])]), e("div", {
                        staticClass: "col-md-3"
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [t._v(t._s(t.hierarchy[t.hierarchy.indexOf(a.type) - 1]))])]), e("hr", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: s < t.organizations.length,
                            expression: "i < organizations.length"
                        }],
                        attrs: {
                            size: "30px"
                        }
                    })])
                }))]) : t._e()])]), null != t.matches && t.countries.length > 0 ? e("div", {
                    staticClass: "row"
                }, [e("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.countries.length > 0,
                        expression: "countries.length > 0"
                    }],
                    staticClass: "col-md-12 shadow"
                }, [e("table", [e("tr", [e("td", {
                    attrs: {
                        align: "left"
                    }
                }, [e("strong", [t._v("Lander")]), t._v("\n            med matchning på " + t._s(t.searchInput) + " ( " + t._s(t.countries.length) + " stycken)\n          ")]), e("td", {
                    attrs: {
                        align: "right"
                    }
                }, [e("i", {
                    staticClass: "fa fa-globe"
                }), e("br"), e("span", {
                    attrs: {
                        slot: "left"
                    },
                    slot: "left"
                }, [t._v("Country")])])]), t._m(1), t._l(t.countries, function (a) {
                    return e("tr", {
                        key: a.link,
                        attrs: {
                            valign: "top"
                        }
                    }, [e("td", {
                        attrs: {
                            align: "left"
                        }
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [e("a", {
                        attrs: {
                            href: a.link
                        }
                    }, [e("strong", [t._v(t._s(a.name))])])]), e("p", [t._v(t._s(t.formatPath(a.path)))])]), e("td", {
                        attrs: {
                            align: "right"
                        }
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [t._v(t._s(a.type))])])])
                })], 2)])]) : t._e(), e("pre", [t._v("    ")]), t.counties.length > 0 ? e("div", {
                    staticClass: "row"
                }, [e("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.counties.length > 0,
                        expression: "counties.length > 0"
                    }],
                    staticClass: "col-md-12 shadow"
                }, [e("table", [e("tr", [e("td", {
                    attrs: {
                        align: "left"
                    }
                }, [e("strong", [t._v("Län")]), t._v("\n            med matchning på " + t._s(t.searchInput) + " ( " + t._s(t.counties.length) + " stycken)\n          ")]), e("td", {
                    attrs: {
                        align: "right"
                    }
                }, [e("i", {
                    staticClass: "fa fa-hospital-o"
                }), e("br"), e("span", {
                    attrs: {
                        slot: "left"
                    },
                    slot: "left"
                }, [t._v("Locality")])])]), t._m(2), t._l(t.counties, function (a) {
                    return e("tr", {
                        key: a.link,
                        attrs: {
                            valign: "top"
                        }
                    }, [e("td", {
                        attrs: {
                            align: "left"
                        }
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [e("a", {
                        attrs: {
                            href: a.link
                        }
                    }, [e("strong", [t._v(t._s(a.name))])])]), e("p", [t._v(t._s(t.formatPath(a.path)))])]), e("td", {
                        attrs: {
                            align: "right"
                        }
                    }, [e("span", {
                        attrs: {
                            slot: "left"
                        },
                        slot: "left"
                    }, [t._v(t._s(a.type))])])])
                })], 2)])]) : t._e(), e("pre", [t._v("    ")]), t.organizations.length > 0 ? e("div", {
                    staticClass: "row"
                }) : t._e()])
            },
            k = [function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("button", {
                    staticClass: "close",
                    attrs: {
                        type: "button",
                        "data-dismiss": "alert",
                        "aria-label": "Close"
                    }
                }, [e("span", {
                    attrs: {
                        "aria-hidden": "true"
                    }
                }, [t._v("×")])])
            }, function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("tr", [e("td", {
                    attrs: {
                        align: "right",
                        colspan: "2"
                    }
                }, [e("hr", {
                    attrs: {
                        width: "100%",
                        size: "2"
                    }
                })])])
            }, function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("tr", {
                    attrs: {
                        valign: "top"
                    }
                }, [e("td", {
                    attrs: {
                        align: "right",
                        colspan: "2"
                    }
                }, [e("hr", {
                    attrs: {
                        width: "100%",
                        size: "2"
                    }
                })])])
            }],
            y = (e("3b2b"), e("a481"), function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("div", {
                    staticClass: "container",
                    attrs: {
                        id: "tabs"
                    }
                }, [e("div", {
                    staticClass: "tabs"
                }, [e("a", {
                    class: [1 === t.activetab ? "active" : ""],
                    on: {
                        click: function (a) {
                            t.activetab = 1
                        }
                    }
                }, [t._v("Enheter")]), e("a", {
                    class: [2 === t.activetab ? "active" : ""],
                    on: {
                        click: function (a) {
                            t.activetab = 2
                        }
                    }
                }, [t._v("Personer")]), e("a", {
                    class: [3 === t.activetab ? "active" : ""],
                    on: {
                        click: function (a) {
                            t.activetab = 3
                        }
                    }
                }, [t._v("Medarbetareuppdrag")])]), e("div", {
                    staticClass: "content"
                }, [1 === t.activetab ? e("div", {
                    staticClass: "tabcontent"
                }, [t._v("\n      Content for tab one\n    ")]) : t._e(), 2 === t.activetab ? e("div", {
                    staticClass: "tabcontent"
                }, [t._v("\n      Content for tab two\n    ")]) : t._e(), 3 === t.activetab ? e("div", {
                    staticClass: "tabcontent"
                }, [t._v("\n      Content for tab three\n    ")]) : t._e()])])
            }),
            x = [],
            S = {
                name: "tabbedResults",
                data: function () {
                    return {
                        activetab: 1
                    }
                }
            };
        console.log("inside tabbed content");
        var M = S,
            O = (e("0abd"), Object(u["a"])(M, y, x, !1, null, null, null));
        O.options.__file = "tabbedResult.vue";
        var z = O.exports;
        s["a"].component("tabbed-results", z);
        var I = {
                name: "resultsTable",
                row: {
                    name: "",
                    path: "",
                    type: "",
                    link: ""
                },
                matches: [{}],
                searchInput: "",
                readMore: !1,
                countries: [{}],
                counties: [{}],
                organizations: [{}],
                units: [{}],
                persons: [{}],
                otherMatches: [{}],
                hierarchy: [],
                props: {
                    name: "name property"
                },
                data: function () {
                    return {
                        matches: [],
                        searchInput: "",
                        readMore: !1,
                        countries: [],
                        counties: [],
                        organizations: [],
                        units: [],
                        persons: [],
                        otherMatches: [],
                        activetab: 1,
                        hierarchy: ["Country", "County", "Municipality", "Organization", "Unit", "Person"]
                    }
                },
                mounted: function () {
                    var t = this;
                    Y.$on("searchInputValue", function (a) {
                        t.searchInput = a
                    })
                },
                created: function () {
                    var t = this;
                    Y.$on("searchInputValue", function (a) {
                        t.searchInput = a, alert("Evnet received - searchInput changed")
                    })
                },
                methods: {
                    getOrganizations: function () {
                        var t = this;
                        this.countries = [], this.counties = [], this.organizations = [], this.units = [], this.persons = [], this.otherMatches = [], this.matches = [], "" !== this.searchInput && null !== this.searchInput ? (this.$http.get("http://127.0.0.1:9080/organization/resources/search/" + this.searchInput.trim() + "?offset=0&size=200").then(function (a) {
                            if (t.matches = JSON.parse(JSON.stringify(a.body.content)), null != t.matches)
                                for (var e = 0; e < t.matches.length; e++) switch (t.matches[e] = JSON.parse(t.matches[e]), t.matches[e].type.toLowerCase()) {
                                    case "country":
                                        -1 === t.countries.indexOf(t.matches[e]) ? t.countries.push(t.matches[e]) : console.log(t.matches[e] + " already exists");
                                        break;
                                    case "locality":
                                        -1 === t.counties.indexOf(t.matches[e]) ? t.counties.push(t.matches[e]) : console.log(t.matches[e] + " already exists");
                                        break;
                                    case "organization":
                                        -1 === t.organizations.indexOf(t.matches[e]) ? t.organizations.push(t.matches[e]) : console.log(t.matches[e] + " already exists");
                                        break;
                                    case "unit":
                                        -1 === t.units.indexOf(t.matches[e]) ? t.units.push(t.matches[e]) : console.log(t.matches[e] + " already exists");
                                        break;
                                    case "person":
                                        -1 === t.persons.indexOf(t.matches[e]) ? t.persons.push(t.matches[e]) : console.log(t.matches[e] + " already exists");
                                        break;
                                    default:
                                        -1 === t.otherMatches.indexOf(t.matches[e]) ? t.otherMatches.push(t.matches[e]) : console.log(t.matches[e] + " already exists");
                                        break
                                }
                        }), this.$http.get("http://localhost:9082/employee/resources/search/" + this.searchInput.trim() + "?offset=0&size=200").then(function (a) {
                            if (t.matches = JSON.parse(JSON.stringify(a.body.content)), null != t.matches)
                                for (var e = 0; e < t.matches.length; e++) switch (t.matches[e] = JSON.parse(t.matches[e]), t.matches[e].type.toLowerCase()) {
                                    case "person":
                                        -1 === t.persons.indexOf(t.matches[e]) ? t.persons.push(t.matches[e]) : console.log(t.matches[e] + " already exists");
                                        break;
                                    default:
                                        -1 === t.otherMatches.indexOf(t.matches[e]) ? t.otherMatches.push(t.matches[e]) : console.log(t.matches[e] + " already exists");
                                        break
                                }
                        })) : this.matches = []
                    },
                    showReadMore: function () {
                        this.readMore = !this.readMore
                    },
                    formatPath: function (t) {
                        return t.replace(new RegExp("/", "g"), " >> ").replace("country >> SE >> county >> ", "")
                    }
                },
                watch: {
                    searchInput: function (t, a) {
                        ("" !== t || t.length > 0) && this.getOrganizations()
                    }
                }
            },
            L = I,
            H = (e("803a"), Object(u["a"])(L, A, k, !1, null, null, null));
        H.options.__file = "resultsTable.vue";
        var N = H.exports,
            E = function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("div", {
                    staticClass: "Login"
                }, [e("h1", [t._v("Login")]), e("div", [e("input", {
                    attrs: {
                        type: "text",
                        name: "username",
                        placeholder: "Username"
                    }
                }), e("input", {
                    attrs: {
                        type: "password",
                        name: "password",
                        placeholder: "Password"
                    }
                }), e("button", {
                    on: {
                        click: t.logIn
                    }
                }, [t._v(t._s(t.$t("login")))])])])
            },
            P = [],
            R = {
                name: "Login",
                methods: {
                    logIn: function () {
                        this.$emit("Login::loginResult", {
                            loginResult: !0
                        }), this.$router.push("admin")
                    }
                }
            },
            $ = R,
            F = Object(u["a"])($, E, P, !1, null, null, null);
        F.options.__file = "login.vue";
        var j = F.exports,
            T = function () {
                var t = this,
                    a = t.$createElement,
                    e = t._self._c || a;
                return e("div", {
                    attrs: {
                        id: "app"
                    }
                }, [t.isLoggedIn ? e("div", [t._v(" Welcome! You are now logged in.")]) : e("Login", {
                    on: {
                        "Login::loginResult": t.handleLoginResult
                    }
                })], 1)
            },
            B = [],
            J = {
                name: "app",
                components: {
                    Login: j
                },
                data: function () {
                    return {
                        userIsLoggedIn: !1
                    }
                },
                computed: {
                    isLoggedIn: function () {
                        return this.userIsLoggedIn
                    }
                },
                methods: {
                    handleLoginResult: function (t) {
                        var a = t.loginResult;
                        this.userIsLoggedIn = a
                    }
                }
            },
            U = J,
            D = Object(u["a"])(U, T, B, !1, null, null, null);
        D.options.__file = "admin.vue";
        var W = D.exports;
        s["a"].use(C["a"]);
        var G = new C["a"]({
                routes: [{
                    path: "/",
                    name: "Home",
                    component: N,
                    meta: {
                        breadcrumb: "Hsa"
                    }
                }, {
                    path: "/login",
                    name: "login",
                    component: j,
                    meta: {
                        breadcrumb: "Login"
                    }
                }, {
                    path: "/admin",
                    name: "admin",
                    component: W,
                    meta: {
                        breadcrumb: "Login"
                    }
                }, {
                    path: "/search",
                    name: "search",
                    component: N,
                    meta: {
                        breadcrumb: {
                            label: "search",
                            parent: "Home"
                        }
                    },
                    children: [{
                        path: "organizations",
                        name: "organizations",
                        meta: {
                            linkText: "organisationer"
                        }
                    }]
                }, {
                    path: "/table",
                    name: "table",
                    component: N,
                    meta: {
                        breadcrumb: {
                            label: "Sök resultat",
                            parent: "Home"
                        }
                    }
                }, {
                    path: "/tabbed",
                    name: "tabbed",
                    component: z,
                    meta: {
                        breadcrumb: {
                            label: "Sök resultat",
                            parent: "Home"
                        }
                    }
                }]
            }),
            V = e("28dd");
        e("4989");
        e.d(a, "eventBus", function () {
            return Y
        }), e("1f54");
        var Y = new s["a"];
        s["a"].config.productionTip = !1, s["a"].use(V["a"]), new s["a"]({
            router: G,
            render: function (t) {
                return t(w)
            }
        }).$mount("#app")
    },
    "64a9": function (t, a, e) {},
    "803a": function (t, a, e) {
        "use strict";
        var s = e("dd80"),
            n = e.n(s);
        n.a
    },
    8489: function (t, a) {
        t.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAA8CAMAAABGivqtAAAAZlBMVEXc2dbc2dfd2dfc2tfe2tfd2dfe29fm5uYAAADc2dbd2tbd29bc3Nzc2dbd2tfe2trd2dbf29fd2tfg4Nbd2tfc3Nnd2tfe2df/29vc2tfn29vf29fJw76pnJLMxcDIwbyjlouilYqT8cRsAAAAHHRSTlP57N/HoHlGCgD/wnAW+7M+w0CmGfBJ/XMHxhU/CpULTwAAANJJREFUeAGl1VPChgAQheHJnc/Ibv+L/G286dw+uYGZ43p+EEbxnzG9xnb7A7Ck4+lMLOlyRZZud2QlKbKyHFnyC2SVFbLKAlk+s3LmLEVWckfWjVlX5guzaubTD27attNnjodv3A/j2OpL9l/5Wb/z7pm/6tB8ZYvsu/b6ltBIFRipfCOVZ6Ryn7n5T+U8c/ufyqaZLz75aPxi4P70RwUPp38olwMXE5ciFzLyeUsTXZFvG9o/SzeMHn/D2OOhiSOXBzaPe1wWtGqOp3rxmuMl+QjGsUvZpiDWtQAAAABJRU5ErkJggg=="
    },
    b738: function (t, a, e) {},
    bad0: function (t, a) {
        t.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAA8CAMAAABGivqtAAAAZlBMVEXc2dbc2dfd2dfc2tfe2tfd2dfe29fm5uYAAADc2dbd2tbd29bc3Nzc2dbd2tfe2trd2dbf29fd2tfg4Nbd2tfc3Nnd2tfe2df/29vc2tfn29vf29fMxcCpnJLJw76ilYqjlovIwbww8noYAAAAHHRSTlP57N/HoHlGCgD/wnAW+7M+w0CmGfBJ/XMHxhU/CpULTwAAANFJREFUeAGk1FWChVAMg+FeheDuuv9Fjrv8I6evH04as8PxdL5cPf/LMT2OBWEELClOUmJJWY4sFSWyqhpZTYssnTtk9QOy+g5ZZ2a1zE2NrKpEVsGsnDlj1sicPPI0z8tXHEcPPK/rtn/l4TN/7cEDL9s3bt49a//Orybyi4n8bCI/mciPpve+vOOD6b3P79h+zXxxfjR+Mf4s/FH5l/AP5ThwmDiKHGTk1GWJcuTCYf2b2qF6zg61x6WJlcuFzXV/OyFhMqsaXj5pUqs5ApUkACPRS6klsTDFAAAAAElFTkSuQmCC"
    },
    dadb: function (t, a, e) {},
    dd80: function (t, a, e) {}
});
//# sourceMappingURL=app.f96765d2.js.map